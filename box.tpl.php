
<div class="content<?php echo $layoutcode ?>-container line-box">
  <div class="content<?php echo $layoutcode ?>-container-1col">
    <p class="content-title-shade-size2 bg-blue07 txt-white"><?php print $title ?></p>
    <div class="content-txtbox-noshade">
      <?php print ($content) ?>
    </div>
  </div>
</div>
